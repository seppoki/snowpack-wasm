# snowpack-wasm

This project builds and runs a web assembly parser (Pest) genereted by rust (wasm-bindgen) in a snowpack bundled svelte project.

The snowpack-plugin-wasm-pack correctly detects chenges and rebuilds the rust project while the snowpack dev server serves the web app.
